module App
    module ActiveRecord
      class Validate
        MIN_NAME_LENGTH = 5
        ERRORS = {
          name_length: "Name length should be at least #{MIN_NAME_LENGTH} characters",
          lastname_presence: "Last name should be present"
        }
  
        def self.call(**args)
          new.call(**args)
        end
  
        # @return [<Boolean>, <Array>] validation result
        # <Boolean> - validation successfull/unsuccessfull
        # <Array> - errors list
        def call(record:)
          validation_result = validate(record:)
  
          validation_result
        end
  
        private
  
        def validate(record:)
          validate_name(record.name)
          validate_lastname(record.lastname)
  
          [
            errors.empty?,
            errors
          ]
        end
  
        def errors
          @errors ||= []
        end
  
        def error(error_name)
          ERRORS.fetch(error_name)
        end
    
        def validate_name(name)
          errors << error(:name_length) unless name.to_s.length >= MIN_NAME_LENGTH
        end
    
        def validate_lastname(lastname)
          errors << error(:lastname_presence) if lastname == nil
        end
      end
    end
  end
  
  
  module App
    module ActiveRecord
      class Persist
        def self.call(record:)
          new.call(record:)
        end
  
        def call(record:)
          valid, errors = App::ActiveRecord::Validate.call(record:)
  
          # puts "For name: #{record&.name}, lastname: #{record&.lastname}"
          if valid
            persist
            handle_success
          else
            handle_failure(errors)
          end
        end
  
        private
  
        def persist
          true
        end
    
        def handle_success
          "Successfully saved.\n\n"
        end
    
        def handle_failure(errors)
          errors_messages = errors.join(",\n")
          "Persisting failed. Validation errors:\n#{errors_messages}\n\n" # TODO: Use HEREDOC
        end
      end
    end
  end
  
  module App
    class Cat
      attr_reader :name, :lastname
      private_class_method :new
  
      def initialize(**args)
        @name = args[:name] if args[:name]
        @lastname = args[:lastname] if args[:lastname]
      end
  
      def self.create(**args)
        record = new(**args) 
        
        App::ActiveRecord::Persist.call(record:)
      end
    end
  end
  
  puts App::Cat.create(name: 'A')
  puts App::Cat.create(name: 'Mindy')
  puts App::Cat.create(lastname: 'Vitalinovna')
  puts App::Cat.create(name: 'Mindy', lastname: 'Vitalinovna')
  